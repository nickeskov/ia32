#!/bin/bash

REPORTS_DIR=reports
ITER_COUNT=100

make
mkdir -p $REPORTS_DIR
rm -rf $REPORTS_DIR
mkdir -p $REPORTS_DIR

function test {
	echo $ITER_COUNT > $REPORTS_DIR/report_$1.txt
	echo "std" "ft" >> $REPORTS_DIR/report_$1.txt
	std_summary=0
	ft_summary=0
	for ((i = 1; i <= $ITER_COUNT; ++i))
	do
		report_std=$(./test_std_memcpy.out $1)
		report_ft=$(./test_ft_memcpy.out $1)
		echo $report_std $report_ft >> $REPORTS_DIR/report_$1.txt

		std_summary=$((std_summary + report_std))
		ft_summary=$((ft_summary + report_ft))
	done
	echo "#######"
	summary="$1"
	summary=$(echo "$summary |" "std =" $(echo $std_summary / $ITER_COUNT | bc -l))
	summary=$(echo "$summary |" "ft =" $(echo $ft_summary / $ITER_COUNT | bc -l))
	summary=$(echo "$summary |" "std / ft =" \
		$(echo $std_summary / $ft_summary | bc -l | sed "s/^\./0./"))

	echo $summary
	echo $summary >> $REPORTS_DIR/summary.txt
	echo "#######"
}

test 16
test 700
test $((16*1024))
test $((4*1024*1024))
