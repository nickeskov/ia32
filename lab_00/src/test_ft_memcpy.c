#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define KbytesToBytes(x) ((x) * 1024)
#define MbytesToBytes(x) ((KbytesToBytes(x)) * 1024)

typedef unsigned char		t_uchar;
typedef unsigned long long	t_ullolg;

static inline t_ullolg	rdtsc(void)
{
	unsigned int lo;
	unsigned int hi;

	__asm__ __volatile__ ( "rdtsc" : "=a" (lo), "=d" (hi) );
	return (((t_ullolg)hi << 32) | lo);
}

static inline void		ft_membtcpy(void *restrict dest,
									const void *restrict src,
									size_t n)
{
	const t_uchar	*restrict sr;
	t_uchar			*restrict ds;

	sr = (const t_uchar*)src;
	ds = (t_uchar*)dest;
	while (n != 0)
	{
		*ds = *sr;
		++ds;
		++sr;
		--n;
	}
}

void					*ft_memcpy(void *restrict dest,
									const void * restrict src,
									size_t n)
{
	const size_t	*restrict sr;
	size_t			*restrict ds;

	if (dest != src)
	{
		ft_membtcpy(dest, src, n % sizeof(size_t));
		sr = (const size_t*)((t_uchar*)src + n % sizeof(size_t));
		ds = (size_t*)((t_uchar*)dest + n % sizeof(size_t));
		n = (n - n % sizeof(size_t)) / sizeof(size_t);
		while (n != 0)
		{
			*ds = *sr;
			++ds;
			++sr;
			--n;
		}
	}
	return (dest);
}

int				main(int argc, char **argv)
{
	t_ullolg	before;
	t_ullolg	diff;
	void	*a;
	void	*b;
	size_t	n;

	if (argc != 2)
		return (1);

	n = strtoul(argv[1], NULL, 10);
	if (n == 0)
		return (1);

	a = malloc(n);
	b = malloc(n);
	if (!a || !b)
	{
		free(a);
		free(b);
		return (1);
	}

	before = rdtsc();
	ft_memcpy(a, b, n);
	diff = rdtsc() - before;

	for (size_t i = 0; i < n; ++i)
		*((unsigned char *) a + i) ^= *((unsigned char *) b + i);
	printf("%llu\n", diff);

	free(a);
	free(b);
	return (0);
}
